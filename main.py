#!/usr/bin/python3
import datetime
from mastodon import Mastodon
import requests
import pytz
import config

def log(input):
    print(f"{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {input}")

timezone = pytz.timezone('Europe/Berlin')

log("Starting.")
masto = Mastodon(
    access_token = config.mastodon_access_token,
    api_base_url = config.mastodon_address,
    ratelimit_method='wait')

timeline_with_hashtag = masto.timeline_hashtag(hashtag="kalkspaceleaks")
to_publish = []
aware_datetime = timezone.localize(datetime.datetime.now())

for status in timeline_with_hashtag:
    if status.created_at > aware_datetime - datetime.timedelta(minutes=config.interval_minutes):
        to_publish.append(status.uri)

if len(to_publish) == 0:
    log("Nothing to post, I'm done!")

for status in to_publish:
    log("Posting: " + status)
    posted = requests.post(config.hook, json={"link": status})
    log("Result: " + posted.status_code)
